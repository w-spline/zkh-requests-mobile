package com.wsplite.zkhrequestsmobile.domain;

import androidx.annotation.DrawableRes;

import com.wsplite.zkhrequestsmobile.R;
import com.wsplite.zkhrequestsmobile.data.api.ApiFactory;
import com.wsplite.zkhrequestsmobile.data.model.CategoryResponse;
import com.wsplite.zkhrequestsmobile.data.model.MainCategoryResponse;
import com.wsplite.zkhrequestsmobile.ui.main.category.item.MainCategoryItem;
import com.wsplite.zkhrequestsmobile.ui.subcategory.CategoryItem;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;

/**
 * Created by Evgeniy Mezentsev on 2019-07-28.
 * email: emezentsev@ccsteam.ru
 */
public class CategoriesInteractor {

    private final static String HOME = "дом";
    private final static String APARTMENT = "квартира";
    private final static String ENTRANCE = "подъезд";
    private final static String YARD = "двор";

    public Observable<List<MainCategoryItem>> getMainCategories() {
        return ApiFactory.INSTANCE.getCategoriesApi().getMainCategories()
                .map(this::mapMain);
    }

    public Observable<List<CategoryItem>> getListRequest(String id) {
        return ApiFactory.INSTANCE.getCategoriesApi().getCategories(id)
                .map(this::map);
    }

    public Observable<List<CategoryItem>> getListRequest() {
        return ApiFactory.INSTANCE.getCategoriesApi().getCategories()
                .map(this::map);
    }

    private List<MainCategoryItem> mapMain(List<MainCategoryResponse> responses) {
        int index = 1;
        List<MainCategoryItem> items = new ArrayList<>();
        for (MainCategoryResponse response : responses) {
            boolean isGreen = index % 2 == 1;
            items.add(new MainCategoryItem(response.getId(), response.getValue(),
                    getDrawable(response.getValue()), getIcon(response.getValue()), isGreen));
            index++;
        }
        return items;
    }

    private List<CategoryItem> map(List<CategoryResponse> responses) {
        List<CategoryItem> items = new ArrayList<>();
        for (CategoryResponse response : responses) {
            items.add(new CategoryItem(response.getId(), response.getValue()));
        }
        return items;
    }

    private @DrawableRes int getDrawable(String value) {
        switch (value.toLowerCase()) {
            case HOME:
                return R.drawable.background_house;
            case APARTMENT:
                return R.drawable.background_apartment;
            case ENTRANCE:
                return R.drawable.background_entrance;
            case YARD:
                return R.drawable.background_yard;
            default:
                return R.drawable.background_yard;
        }
    }

    private @DrawableRes int getIcon(String value) {
        switch (value.toLowerCase()) {
            case HOME:
                return R.drawable.ic_home;
            case APARTMENT:
                return R.drawable.ic_apartment;
            case ENTRANCE:
                return R.drawable.ic_entrance;
            case YARD:
                return R.drawable.ic_yard;
            default:
                return R.drawable.ic_yard;
        }
    }
}
