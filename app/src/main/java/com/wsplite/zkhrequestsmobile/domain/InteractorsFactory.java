package com.wsplite.zkhrequestsmobile.domain;

/**
 * Created by Evgeniy Mezentsev on 2019-07-28.
 * email: emezentsev@ccsteam.ru
 */
public enum InteractorsFactory {

    INSTANCE;

    private CategoriesInteractor categoriesInteractor;

    InteractorsFactory() {
        categoriesInteractor = new CategoriesInteractor();
    }

    public CategoriesInteractor getCategoriesInteractor() {
        return categoriesInteractor;
    }
}
