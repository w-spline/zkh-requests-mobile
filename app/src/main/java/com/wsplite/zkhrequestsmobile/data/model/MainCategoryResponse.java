package com.wsplite.zkhrequestsmobile.data.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Evgeniy Mezentsev on 2019-07-28.
 * email: emezentsev@ccsteam.ru
 */
public class MainCategoryResponse {

    @SerializedName("id")
    private String id;

    @SerializedName("value")
    private String value;

    public MainCategoryResponse(String id, String value) {
        this.id = id;
        this.value = value;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MainCategoryResponse that = (MainCategoryResponse) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        return value != null ? value.equals(that.value) : that.value == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (value != null ? value.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "MainCategoryResponse{" +
                "id='" + id + '\'' +
                ", value='" + value + '\'' +
                '}';
    }
}
