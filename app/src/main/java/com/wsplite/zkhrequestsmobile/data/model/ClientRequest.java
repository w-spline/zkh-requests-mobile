package com.wsplite.zkhrequestsmobile.data.model;

/**
 * Created by Evgeniy Mezentsev on 2019-07-28.
 * email: emezentsev@ccsteam.ru
 */
public class ClientRequest {

    public String fam;

    public String im;

    public String ot;

    public String email;

    public String address;

    public String phone;

    public ClientRequest(String fam, String im, String ot, String email, String phone) {
        this.fam = fam;
        this.im = im;
        this.ot = ot;
        this.email = email;
        this.address = address;
        this.phone = phone;
    }
}
