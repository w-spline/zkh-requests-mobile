package com.wsplite.zkhrequestsmobile.data.api;

import com.wsplite.zkhrequestsmobile.data.model.CategoryResponse;
import com.wsplite.zkhrequestsmobile.data.model.IncedentRequest;
import com.wsplite.zkhrequestsmobile.data.model.IncedentResponse;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

/**
 * Created by Evgeniy Mezentsev on 2019-07-28.
 * email: emezentsev@ccsteam.ru
 */
public interface IncedentsApi {

    @POST("/incidents")
    Observable<IncedentResponse> postIncedent(@Body IncedentRequest incedentRequest);

    @GET("/incidents")
    Observable<List<CategoryResponse>> getIncedents();
}
