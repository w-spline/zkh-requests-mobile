package com.wsplite.zkhrequestsmobile.data.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Evgeniy Mezentsev on 2019-07-28.
 * email: emezentsev@ccsteam.ru
 */
public class IncedentRequest {

    @SerializedName("client")
    public String client;

    @SerializedName("sub_category")
    public String subCategory;

    @SerializedName("issue_descr")
    public String issueDescr;

    @SerializedName("address")
    public String address;

    public IncedentRequest(String client, String subCategory, String issueDescr, String address) {
        this.client = client;
        this.subCategory = subCategory;
        this.issueDescr = issueDescr;
        this.address = address;
    }
}
