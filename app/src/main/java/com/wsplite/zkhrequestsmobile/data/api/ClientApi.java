package com.wsplite.zkhrequestsmobile.data.api;

import com.wsplite.zkhrequestsmobile.data.model.ClientRequest;
import com.wsplite.zkhrequestsmobile.data.model.ClientResponse;


import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Created by Evgeniy Mezentsev on 2019-07-28.
 * email: emezentsev@ccsteam.ru
 */
public interface ClientApi {

    @POST("/client/new_client")
    Observable<ClientResponse> registration(@Body ClientRequest clientRequest);

}
