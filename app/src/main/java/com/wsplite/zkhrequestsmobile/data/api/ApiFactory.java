package com.wsplite.zkhrequestsmobile.data.api;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Evgeniy Mezentsev on 2019-07-28.
 * email: emezentsev@ccsteam.ru
 */
public enum ApiFactory {

    INSTANCE;

    private final IncedentsApi incedentsApi;
    private final CategoriesApi categoriesApi;
    private final ClientApi clientApi;

    ApiFactory() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://192.168.43.185:8000/")
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
        incedentsApi = retrofit.create(IncedentsApi.class);
        categoriesApi = retrofit.create(CategoriesApi.class);
        clientApi = retrofit.create(ClientApi.class);
    }

    public IncedentsApi getIncedentsApi() {
        return incedentsApi;
    }

    public CategoriesApi getCategoriesApi() {
        return categoriesApi;
    }

    public ClientApi getClientApi() {
        return clientApi;
    }}
