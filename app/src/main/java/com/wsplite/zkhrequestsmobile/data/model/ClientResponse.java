package com.wsplite.zkhrequestsmobile.data.model;

/**
 * Created by Evgeniy Mezentsev on 2019-07-28.
 * email: emezentsev@ccsteam.ru
 */
public class ClientResponse {

    public String id;

    public ClientResponse(String id) {
        this.id = id;
    }
}
