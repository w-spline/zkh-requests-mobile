package com.wsplite.zkhrequestsmobile.data.api;

import com.wsplite.zkhrequestsmobile.data.model.CategoryResponse;
import com.wsplite.zkhrequestsmobile.data.model.MainCategoryResponse;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Evgeniy Mezentsev on 2019-07-28.
 * email: emezentsev@ccsteam.ru
 */
public interface CategoriesApi {

    @GET("/issues/categories")
    Observable<List<MainCategoryResponse>> getMainCategories();

    @GET("/issues/sub_categories")
    Observable<List<CategoryResponse>> getCategories(@Query("category") String category);

    @GET("/issues/all_sub_category")
    Observable<List<CategoryResponse>> getCategories();
}
