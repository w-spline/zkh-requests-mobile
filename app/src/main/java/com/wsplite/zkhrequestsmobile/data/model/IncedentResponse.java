package com.wsplite.zkhrequestsmobile.data.model;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

/**
 * Created by Evgeniy Mezentsev on 2019-07-28.
 * email: emezentsev@ccsteam.ru
 */
public class IncedentResponse {

    @SerializedName("id")
    private String id;

    @SerializedName("date")
    private Date date;

    @SerializedName("last_name")
    private String lastName;

    @SerializedName("name")
    private String name;

    @SerializedName("name2")
    private String name2;

    @SerializedName("address")
    private String address;

    @SerializedName("email")
    private String email;

    @SerializedName("phone")
    private String phone;

    @SerializedName("category")
    private String category;

    @SerializedName("message")
    private String message;

    @SerializedName("status_history")
    private StatusHistory statusHistory;

    public IncedentResponse(String id, Date date, String lastName, String name,
                            String name2, String address, String email, String phone,
                            String category, String message, StatusHistory statusHistory) {
        this.id = id;
        this.date = date;
        this.lastName = lastName;
        this.name = name;
        this.name2 = name2;
        this.address = address;
        this.email = email;
        this.phone = phone;
        this.category = category;
        this.message = message;
        this.statusHistory = statusHistory;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName2() {
        return name2;
    }

    public void setName2(String name2) {
        this.name2 = name2;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public StatusHistory getStatusHistory() {
        return statusHistory;
    }

    public void setStatusHistory(StatusHistory statusHistory) {
        this.statusHistory = statusHistory;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        IncedentResponse that = (IncedentResponse) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (date != null ? !date.equals(that.date) : that.date != null) return false;
        if (lastName != null ? !lastName.equals(that.lastName) : that.lastName != null) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (name2 != null ? !name2.equals(that.name2) : that.name2 != null) return false;
        if (address != null ? !address.equals(that.address) : that.address != null) return false;
        if (email != null ? !email.equals(that.email) : that.email != null) return false;
        if (phone != null ? !phone.equals(that.phone) : that.phone != null) return false;
        if (category != null ? !category.equals(that.category) : that.category != null) return false;
        if (message != null ? !message.equals(that.message) : that.message != null) return false;
        return statusHistory != null ? statusHistory.equals(that.statusHistory) : that.statusHistory == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (date != null ? date.hashCode() : 0);
        result = 31 * result + (lastName != null ? lastName.hashCode() : 0);
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (name2 != null ? name2.hashCode() : 0);
        result = 31 * result + (address != null ? address.hashCode() : 0);
        result = 31 * result + (email != null ? email.hashCode() : 0);
        result = 31 * result + (phone != null ? phone.hashCode() : 0);
        result = 31 * result + (category != null ? category.hashCode() : 0);
        result = 31 * result + (message != null ? message.hashCode() : 0);
        result = 31 * result + (statusHistory != null ? statusHistory.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "IncedentResponse{" +
                "id='" + id + '\'' +
                ", date=" + date +
                ", lastName='" + lastName + '\'' +
                ", name='" + name + '\'' +
                ", name2='" + name2 + '\'' +
                ", address='" + address + '\'' +
                ", email='" + email + '\'' +
                ", phone='" + phone + '\'' +
                ", category='" + category + '\'' +
                ", message='" + message + '\'' +
                ", statusHistory=" + statusHistory +
                '}';
    }

    public class StatusHistory {

        @SerializedName("last_status")
        private String lastStatus;

        @SerializedName("date_status")
        private Date dateStatus;

        public StatusHistory(String lastStatus, Date dateStatus) {
            this.lastStatus = lastStatus;
            this.dateStatus = dateStatus;
        }

        public String getLastStatus() {
            return lastStatus;
        }

        public void setLastStatus(String lastStatus) {
            this.lastStatus = lastStatus;
        }

        public Date getDateStatus() {
            return dateStatus;
        }

        public void setDateStatus(Date dateStatus) {
            this.dateStatus = dateStatus;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            StatusHistory that = (StatusHistory) o;

            if (lastStatus != null ? !lastStatus.equals(that.lastStatus) : that.lastStatus != null) return false;
            return dateStatus != null ? dateStatus.equals(that.dateStatus) : that.dateStatus == null;
        }

        @Override
        public int hashCode() {
            int result = lastStatus != null ? lastStatus.hashCode() : 0;
            result = 31 * result + (dateStatus != null ? dateStatus.hashCode() : 0);
            return result;
        }

        @Override
        public String toString() {
            return "StatusHistory{" +
                    "lastStatus='" + lastStatus + '\'' +
                    ", dateStatus=" + dateStatus +
                    '}';
        }
    }
}
