package com.wsplite.zkhrequestsmobile.data;

import com.wsplite.zkhrequestsmobile.data.api.CategoriesApi;
import com.wsplite.zkhrequestsmobile.data.api.ClientApi;
import com.wsplite.zkhrequestsmobile.data.api.IncedentsApi;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Evgeniy Mezentsev on 2019-07-28.
 * email: emezentsev@ccsteam.ru
 */
public enum Singleton {

    INSTANCE;

    public String id;
    public String address;


}
