package com.wsplite.zkhrequestsmobile.ui.main.settings.personal;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.wsplite.zkhrequestsmobile.R;

import java.util.Date;

public class MainPersonalActivity extends AppCompatActivity {

    String firstName = "Вася";
    String lastName ="Пупкин";
    String middleТame = "Иванович";
    String phone ="8(4712)343456";
    String date = "23.11.1995";
    String email = "face@mail.ru";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_personal);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ((TextView) findViewById(R.id.tv_fio)).setText(firstName + " " + middleТame + " " + lastName);
        ((TextView) findViewById(R.id.tv_date)).setText(date);
        ((TextView) findViewById(R.id.tv_phone)).setText(phone);
        ((TextView) findViewById(R.id.tv_email)).setText(email);

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_setting_add, menu);
        return true;
    }

    @Override
    public void onBackPressed() {
        Intent data = new Intent();
        setResult(RESULT_OK, data);

        super.finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onMenuHomePressed();
                return true;
            case R.id.action_add:
                //Переход на экран
                Intent intent = new Intent(MainPersonalActivity.this, MainPersonalEditActivity.class);
                startActivityForResult(intent, 1);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    protected void onMenuHomePressed() {
        onBackPressed();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (data == null) {return;}
        String name = data.getStringExtra("name");
        if (name == null) return;
    }
}
