package com.wsplite.zkhrequestsmobile.ui.subcategory;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.wsplite.zkhrequestsmobile.R;

import java.util.List;

/**
 * Created by Evgeniy Mezentsev on 2019-07-27.
 * email: emezentsev@ccsteam.ru
 */
public class CategoriesRvAdapter extends RecyclerView.Adapter<CategoriesRvAdapter.ViewHolder> {

    private List<CategoryItem> categories;
    private OnListInteractionListener mListener;

    public CategoriesRvAdapter(List<CategoryItem> categories, OnListInteractionListener listener) {
        this.categories = categories;
        mListener = listener;
    }

    public void setCategories(List<CategoryItem> categories) {
        this.categories = categories;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.category_item, parent, false);
        return new CategoriesRvAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        String category = categories.get(position).getCategory();
        holder.tvCategory.setText(category);
        holder.llCategories.setOnClickListener(view -> mListener.onClick(categories.get(position)));
    }

    @Override
    public int getItemCount() {
        return categories.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        final View mView;
        final LinearLayout llCategories;
        final TextView tvCategory;

        ViewHolder(View view) {
            super(view);
            mView = view;
            llCategories = view.findViewById(R.id.ll_categories);
            tvCategory = view.findViewById(R.id.tv_category);
        }
    }

    interface OnListInteractionListener {

        void onClick(CategoryItem category);
    }
}
