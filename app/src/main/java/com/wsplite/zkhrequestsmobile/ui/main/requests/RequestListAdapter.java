package com.wsplite.zkhrequestsmobile.ui.main.requests;

import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.wsplite.zkhrequestsmobile.R;
import com.wsplite.zkhrequestsmobile.ui.component.RequestStatusColor;
import com.wsplite.zkhrequestsmobile.ui.main.requests.item.ListRequestItem;

import java.util.List;

public class RequestListAdapter extends RecyclerView.Adapter<RequestListAdapter.ViewHolder> {

    private List<ListRequestItem> mValues;
    //private final RequestListFragment.OnListFragmentInteractionListener mListener;

    public RequestListAdapter(List<ListRequestItem> items) {
        mValues = items;
        //mListener = listener;
    }

    public void setValues(List<ListRequestItem> values) {
        this.mValues = values;
        notifyDataSetChanged();
    }

    @Override
    public RequestListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.request_list_item, parent, false);
        return new RequestListAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final RequestListAdapter.ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        holder.ivIcon.setImageResource(mValues.get(position).getIcon());
        holder.tvTitle.setText(mValues.get(position).getTitle());
        holder.tvText.setText(mValues.get(position).getMessage());
        String status = mValues.get(position).getStatus();
        if (status.length() > 9) status = status.substring(0, 9) + "...";
        holder.tvStatus.setText(status);
        Drawable drawable = ContextCompat.getDrawable(holder.mView.getContext(),
                RequestStatusColor.getBackground(mValues.get(position).getStatus()));
        holder.tvStatus.setBackground(drawable);

        holder.mView.setOnClickListener(v -> {
           /* if (null != mListener) {
                mListener.onListFragmentInteraction(holder.mItem);
            }*/
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        final View mView;
        final ImageView ivIcon;
        final TextView tvTitle;
        final TextView tvText;
        final TextView tvStatus;
        ListRequestItem mItem;

        ViewHolder(View view) {
            super(view);
            mView = view;
            ivIcon = view.findViewById(R.id.iv_icon);
            tvTitle = view.findViewById(R.id.tv_title);
            tvText = view.findViewById(R.id.tv_text);
            tvStatus = view.findViewById(R.id.tv_status);
        }
    }
}
