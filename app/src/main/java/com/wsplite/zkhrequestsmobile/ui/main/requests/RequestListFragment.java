package com.wsplite.zkhrequestsmobile.ui.main.requests;

import android.content.Context;
import android.os.Bundle;

import androidx.appcompat.widget.SearchView;
import androidx.core.view.MenuItemCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.wsplite.zkhrequestsmobile.R;
import com.wsplite.zkhrequestsmobile.ui.main.requests.item.ListRequestItem;
import com.wsplite.zkhrequestsmobile.ui.request.RequestActivity;

import java.util.ArrayList;
import java.util.List;

public class RequestListFragment extends Fragment {

  //  private RequestListFragment.OnListFragmentInteractionListener mListener;
    private RequestListAdapter adapter;
    private List<ListRequestItem> list;

    public static RequestListFragment newInstance() {
        RequestListFragment fragment = new RequestListFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_list_request, container, false);

        list = new ArrayList<>();
        list.add(new ListRequestItem(R.drawable.ic_apartment_requests, "Плесень на стенах", "Новая", "Только недавно сделали ремонт и бла бла бла и бла бла бла"));
        list.add(new ListRequestItem(R.drawable.ic_home_requests, "Грязные стены", "Выполнено", "Помогите " +
                "осыпается и сделатйте " +
                "ремонт и" +
                " бла бла бла и бла бла бла"));
        list.add(new ListRequestItem(R.drawable.ic_entrance_requests, "Надписи на стенах", "Продлена", "Надписи " +
                "сделали вандалы" +
                " " +
                "" +
                " " +
                " и бла бла бла и бла бла бла"));
        list.add(new ListRequestItem(R.drawable.ic_home_requests, "Не убран снег", "Зарегестрировано", "Дворник не " +
                "рабоает, очень странное поведение"));
        list.add(new ListRequestItem(R.drawable.ic_entrance_requests, "Повреждены ступени", "Отклонено", "Только " +
                "недавно сделали " +
                "ремонт и бла бла бла и бла бла бла"));
        list.add(new ListRequestItem(R.drawable.ic_apartment_requests, "Плесень на стенах", "Новая", "Только недавно сделали ремонт и бла бла бла и бла бла бла"));
        list.add(new ListRequestItem(R.drawable.ic_home_requests, "Осыпается балкон", "Новая", "Только недавно " +
                "сделали ремонт и бла бла бла и бла бла бла"));
        list.add(new ListRequestItem(R.drawable.ic_apartment_requests, "Плесень на стенах", "Новая", "Только недавно сделали ремонт и бла бла бла и бла бла бла"));

        // Set the adapter
        Context context = view.getContext();
        RecyclerView recyclerView = view.findViewById(R.id.list);
        adapter = new RequestListAdapter(list);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        recyclerView.setAdapter(adapter);
        setHasOptionsMenu(true);
        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.search, menu);
        final MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) searchItem.getActionView();

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (list != null) {
                    List<ListRequestItem> items = new ArrayList<>();
                    for (ListRequestItem item : list) {
                        if ((item.getTitle() + item.getMessage()).toLowerCase().contains(newText.toLowerCase())) {
                            items.add(item);
                        }
                    }
                    adapter.setValues(items);
                }
                return true;
            }
        });

        searchView.setOnSearchClickListener(v -> setItemsVisibility(menu, searchItem, false));

        MenuItemCompat.setOnActionExpandListener(searchItem, new MenuItemCompat.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                setItemsVisibility(menu, searchItem, true);
                return true;
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_search:
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
//        if (context instanceof RequestListFragment.OnListFragmentInteractionListener) {
//           // mListener = (RequestListFragment.OnListFragmentInteractionListener) context;
//        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnListFragmentInteractionListener");
//        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        //mListener = null;
    }

    public interface OnListFragmentInteractionListener {
        void onListFragmentInteraction(ListRequestItem item);
    }

    private void setItemsVisibility(Menu menu, MenuItem exception, boolean visible) {
        for (int i = 0; i < menu.size(); ++i) {
            MenuItem item = menu.getItem(i);
            if (item != exception) item.setVisible(visible);
        }
    }
}
