package com.wsplite.zkhrequestsmobile.ui.main.category.item;

import androidx.annotation.DrawableRes;

/**
 * Created by Evgeniy Mezentsev on 2019-07-27.
 * email: emezentsev@ccsteam.ru
 */
public class MainCategoryItem {

    private String id;
    private String text;
    private @DrawableRes int image;
    private @DrawableRes int icon;
    private boolean isGreen;

    public MainCategoryItem(String id, String text, int image, int icon, boolean isGreen) {
        this.id = id;
        this.text = text;
        this.image = image;
        this.icon = icon;
        this.isGreen = isGreen;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public boolean isGreen() {
        return isGreen;
    }

    public void setGreen(boolean green) {
        isGreen = green;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MainCategoryItem item = (MainCategoryItem) o;

        if (image != item.image) return false;
        if (icon != item.icon) return false;
        if (isGreen != item.isGreen) return false;
        if (id != null ? !id.equals(item.id) : item.id != null) return false;
        return text != null ? text.equals(item.text) : item.text == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (text != null ? text.hashCode() : 0);
        result = 31 * result + image;
        result = 31 * result + icon;
        result = 31 * result + (isGreen ? 1 : 0);
        return result;
    }

    @Override
    public String toString() {
        return "MainCategoryItem{" +
                "id='" + id + '\'' +
                ", text='" + text + '\'' +
                ", image=" + image +
                ", icon=" + icon +
                ", isGreen=" + isGreen +
                '}';
    }
}
