package com.wsplite.zkhrequestsmobile.ui.component;

import com.wsplite.zkhrequestsmobile.R;

/**
 * Created by Evgeniy Mezentsev on 2019-07-28.
 * email: emezentsev@ccsteam.ru
 */
public class RequestCategoryIcon {

    private final static String HOME = "дом";
    private final static String APARTMENT = "квартира";
    private final static String ENTRANCE = "подъезд";
    private final static String YARD = "двор";

    public final int getIcon(String category) {
        switch (category) {
            case HOME:
                return R.drawable.ic_home_requests;
            case APARTMENT:
                return R.drawable.ic_apartment_requests;
            case ENTRANCE:
                return R.drawable.ic_entrance_requests;
            case YARD:
                return R.drawable.ic_entrance_requests;
            default:
                return 0;
        }
    }
}
