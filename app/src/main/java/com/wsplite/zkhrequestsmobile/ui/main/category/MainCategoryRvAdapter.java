package com.wsplite.zkhrequestsmobile.ui.main.category;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.wsplite.zkhrequestsmobile.R;
import com.wsplite.zkhrequestsmobile.ui.main.category.MainCategoryFragment.OnListFragmentInteractionListener;
import com.wsplite.zkhrequestsmobile.ui.main.category.item.MainCategoryItem;

import java.util.List;

public class MainCategoryRvAdapter extends RecyclerView.Adapter<MainCategoryRvAdapter.ViewHolder> {

    private List<MainCategoryItem> mValues;
    private final OnListFragmentInteractionListener mListener;

    public MainCategoryRvAdapter(List<MainCategoryItem> items, OnListFragmentInteractionListener listener) {
        mValues = items;
        mListener = listener;
    }

    public void setValues(List<MainCategoryItem> values) {
        this.mValues = values;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.main_category_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        holder.ivCategory.setImageResource(mValues.get(position).getImage());
        holder.ivIcon.setImageResource(mValues.get(position).getIcon());
        holder.tvCategory.setText(mValues.get(position).getText().toLowerCase());
        if (mValues.get(position).isGreen()) {
            int color = ContextCompat.getColor(holder.mView.getContext(), R.color.white);
            holder.tvCategory.setTextColor(color);
        }

        holder.mView.setOnClickListener(v -> {
            if (null != mListener) {
                mListener.onListFragmentInteraction(holder.mItem);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        final View mView;
        final TextView tvCategory;
        final ImageView ivCategory;
        final ImageView ivIcon;
        MainCategoryItem mItem;

        ViewHolder(View view) {
            super(view);
            mView = view;
            ivCategory = view.findViewById(R.id.iv_category);
            tvCategory = view.findViewById(R.id.tv_category);
            ivIcon = view.findViewById(R.id.iv_icon);
        }
    }
}
