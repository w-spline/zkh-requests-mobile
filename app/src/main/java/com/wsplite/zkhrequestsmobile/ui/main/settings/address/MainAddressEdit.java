package com.wsplite.zkhrequestsmobile.ui.main.settings.address;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.GsonBuilder;
import com.wsplite.zkhrequestsmobile.R;
import com.wsplite.zkhrequestsmobile.data.Singleton;
import com.wsplite.zkhrequestsmobile.data.api.ApiFactory;
import com.wsplite.zkhrequestsmobile.data.model.ClientRequest;
import com.wsplite.zkhrequestsmobile.ui.main.MainActivity;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

import static com.wsplite.zkhrequestsmobile.data.Contstants.PREF_FILE_NAME;

public class MainAddressEdit extends AppCompatActivity {

    public final static String MODE_EXTRA = "mode_extra";
    public final static String USER_EXTRA = "user_extra";

    public final static int MODE_DEFAULT = 0;
    public final static int MODE_REG = 1;

    private int mode = 0;
    private ClientRequest clientRequest;

    private EditText obl;
    private EditText town;
    private EditText street;
    private EditText number;
    private EditText kv;
    private EditText index;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_address_edit);
        Toolbar toolbar = findViewById(R.id.toolbar);
        mode = getIntent().getIntExtra(MODE_EXTRA, MODE_DEFAULT);
        if (mode == 1) {
            toolbar.setTitle("Регистрация");
            String json = getIntent().getStringExtra(USER_EXTRA);
            clientRequest = new GsonBuilder().create().fromJson(json, ClientRequest.class);
        }
        obl = findViewById(R.id.et_obl);
        town = findViewById(R.id.et_town);
        street = findViewById(R.id.et_street);
        number = findViewById(R.id.et_number);
        kv = findViewById(R.id.et_kv);
        index = findViewById(R.id.et_index);
        setSupportActionBar(toolbar);

        getSupportActionBar().setHomeButtonEnabled(mode == 0);
        getSupportActionBar().setDisplayHomeAsUpEnabled(mode == 0);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_accept, menu);
        return true;
    }

    public void onBackPressed() {
        Intent data = new Intent();
        setResult(RESULT_OK, data);

        super.finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onMenuHomePressed();
                return true;
            case R.id.action_accept:
                //Переход на экран
                if (mode == 0) {
                    Intent data = new Intent();
                    setResult(RESULT_OK, data);
                } else {
                    clientRequest.address = obl.getText() + " " + town.getText() + " " + street + " " +
                            number.getText() + " " + kv.getText() + " " + index.getText();
                    Singleton.INSTANCE.address = clientRequest.address;
                    ApiFactory.INSTANCE.getClientApi().registration(clientRequest)
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(
                                    response -> {
                                        Singleton.INSTANCE.id = response.id;
                                        Intent intent = new Intent(this, MainActivity.class);
                                        startActivity(intent);
                                    },
                                    error -> Toast.makeText(this, error.getMessage(), Toast.LENGTH_SHORT).show()
                            );
                }

                super.finish();
        }
        return super.onOptionsItemSelected(item);
    }

    protected void onMenuHomePressed() {
        onBackPressed();
    }

}
