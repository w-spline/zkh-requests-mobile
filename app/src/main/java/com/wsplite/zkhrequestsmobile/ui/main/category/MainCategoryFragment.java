package com.wsplite.zkhrequestsmobile.ui.main.category;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.appcompat.widget.SearchView;
import androidx.core.view.MenuItemCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.wsplite.zkhrequestsmobile.R;
import com.wsplite.zkhrequestsmobile.ui.main.category.item.MainCategoryItem;
import com.wsplite.zkhrequestsmobile.ui.request.RequestActivity;

import java.util.ArrayList;
import java.util.List;

public class MainCategoryFragment extends Fragment {

    private OnListFragmentInteractionListener mListener;
    private MainCategoryRvAdapter adapter;
    private List<MainCategoryItem> list;
    private MainCategoryPresenter presenter = new MainCategoryPresenter(this);

    public static MainCategoryFragment newInstance() {
        MainCategoryFragment fragment = new MainCategoryFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main_category, container, false);

        //list = new ArrayList<>();
        //list.add(new MainCategoryItem("квартира", R.drawable.background_apartment, R.drawable.ic_apartment, true));
        //list.add(new MainCategoryItem("подъезд", R.drawable.background_entrance, R.drawable.ic_entrance, false));
        //list.add(new MainCategoryItem("дом", R.drawable.background_house, R.drawable.ic_home, true));
        //list.add(new MainCategoryItem("двор", R.drawable.background_yard, R.drawable.ic_yard, false));

        // Set the adapter
        Context context = view.getContext();
        RecyclerView recyclerView = view.findViewById(R.id.list);
        adapter = new MainCategoryRvAdapter(new ArrayList(), mListener);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        recyclerView.setAdapter(adapter);
        setHasOptionsMenu(true);
        presenter.onCreate();
        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_main, menu);
        final MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) searchItem.getActionView();

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (list != null) {
                    List<MainCategoryItem> items = new ArrayList<>();
                    for (MainCategoryItem item : list) {
                        if (item.getText().contains(newText)) {
                            items.add(item);
                        }
                    }
                    adapter.setValues(items);
                }
                return true;
            }
        });

        searchView.setOnSearchClickListener(v -> setItemsVisibility(menu, searchItem, false));

        MenuItemCompat.setOnActionExpandListener(searchItem, new MenuItemCompat.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                setItemsVisibility(menu, searchItem, true);
                return true;
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_add:
                RequestActivity.start(getContext());
                return true;
            case R.id.action_search:
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListener) {
            mListener = (OnListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.onCreate();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public void setList(List<MainCategoryItem> items) {
        this.list = items;
        adapter.setValues(items);
    }

    public void toast(String text) {
        Toast.makeText(getContext(), text, Toast.LENGTH_SHORT).show();
    }

    public void showLoader() {
        if (getView() != null && (list == null || list.isEmpty())) {
            getView().findViewById(R.id.pb_categories).setVisibility(View.VISIBLE);
        }
    }

    public void hideLoader() {
        if (getView() != null) {
            getView().findViewById(R.id.pb_categories).setVisibility(View.GONE);
        }
    }

    public interface OnListFragmentInteractionListener {
        void onListFragmentInteraction(MainCategoryItem item);
    }

    private void setItemsVisibility(Menu menu, MenuItem exception, boolean visible) {
        for (int i = 0; i < menu.size(); ++i) {
            MenuItem item = menu.getItem(i);
            if (item != exception) item.setVisible(visible);
        }
    }
}
