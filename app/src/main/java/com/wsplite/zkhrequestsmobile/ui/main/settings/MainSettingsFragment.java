package com.wsplite.zkhrequestsmobile.ui.main.settings;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.wsplite.zkhrequestsmobile.R;
import com.wsplite.zkhrequestsmobile.ui.main.settings.address.MainAddress;
import com.wsplite.zkhrequestsmobile.ui.main.settings.personal.MainPersonalActivity;

public class MainSettingsFragment extends Fragment {

    public static MainSettingsFragment newInstance() {
        MainSettingsFragment fragment = new MainSettingsFragment();
        return fragment;
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.main_settings, container, false);

        View btnPersonal = view.findViewById(R.id.btnPersonal);
        btnPersonal.setOnClickListener(v -> {
            Intent intent = new Intent(getContext(), MainPersonalActivity.class);
            startActivity(intent);
        });

        View btnAddress = view.findViewById(R.id.btnAddress);
        btnAddress.setOnClickListener(v -> {
            Intent intent = new Intent(getContext(), MainAddress.class);
            startActivity(intent);
        });

        View btnNotification = view.findViewById(R.id.btnNotification);
        btnNotification.setOnClickListener(v -> {
            Intent intent = new Intent(getContext(), MainNotificationsActivity.class);
            startActivity(intent);
        });

        return view;
    }
}
