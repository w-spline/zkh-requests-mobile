package com.wsplite.zkhrequestsmobile.ui.request;

import android.content.Context;
import android.os.Bundle;
import android.content.Intent;

import com.wsplite.zkhrequestsmobile.R;
import com.wsplite.zkhrequestsmobile.data.Singleton;
import com.wsplite.zkhrequestsmobile.data.api.ApiFactory;
import com.wsplite.zkhrequestsmobile.data.model.IncedentRequest;
import com.wsplite.zkhrequestsmobile.ui.main.MainActivity;
import com.wsplite.zkhrequestsmobile.ui.subcategory.CategoriesActivity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;


public class RequestActivity extends AppCompatActivity {

    private static String CATEGORY = "category";
    private static String ID = "id";

    private TextView selectionCategory;
    private Button bntSendRequest;

    private String idCat;

    public static void start(Context context) {
        Intent intent = new Intent(context, RequestActivity.class);
        context.startActivity(intent);
    }

    public static void start(Context context, String id, String category) {
        Intent intent = new Intent(context, RequestActivity.class);
        intent.putExtra(CATEGORY, category);
        intent.putExtra(ID, id);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_request);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        String cat = getIntent().getStringExtra(CATEGORY);
        idCat = getIntent().getStringExtra(ID);

        if (cat == null) cat = "";
        if (idCat == null) idCat= "";

        selectionCategory = findViewById(R.id.tv_selectable_category);
        bntSendRequest = findViewById(R.id.bntSendRequest);
        Button test = findViewById(R.id.test);
        LinearLayout llSelectCategory = findViewById(R.id.ll_select_category);
        EditText et = findViewById(R.id.editText);
        selectionCategory.setText(cat);

        test.setOnClickListener(v -> {
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
        });
        bntSendRequest.setOnClickListener(v -> {
            ApiFactory.INSTANCE.getIncedentsApi().postIncedent(new IncedentRequest(Singleton.INSTANCE.id,
                    idCat, et.getText().toString(), Singleton.INSTANCE.address))
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(
                            list -> {
                                Intent intent = new Intent(this, MainActivity.class);
                                startActivity(intent);
                            },
                            error -> Toast.makeText(this, error.getMessage(), Toast.LENGTH_LONG)
                    );
        });

        llSelectCategory.setOnClickListener(v -> {
            startActivityForResult(CategoriesActivity.getIntent(this), 1);
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (data == null) {return;}
        String name = data.getStringExtra("name");
        idCat = data.getStringExtra("id");
        if (name == null || idCat == null) return;
        selectionCategory.setText(name.toLowerCase());
    }

}
