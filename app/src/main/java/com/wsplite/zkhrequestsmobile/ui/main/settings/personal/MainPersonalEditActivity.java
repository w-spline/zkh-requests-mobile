package com.wsplite.zkhrequestsmobile.ui.main.settings.personal;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;

import com.google.gson.GsonBuilder;
import com.wsplite.zkhrequestsmobile.R;
import com.wsplite.zkhrequestsmobile.data.model.ClientRequest;
import com.wsplite.zkhrequestsmobile.ui.main.settings.address.MainAddressEdit;

public class MainPersonalEditActivity extends AppCompatActivity {

    public final static String MODE_EXTRA = "mode_extra";

    public final static int MODE_DEFAULT = 0;
    public final static int MODE_REG = 1;

    private int mode = 0;

    private EditText fm;
    private EditText name;
    private EditText ot;
    private EditText date;
    private EditText phone;
    private EditText email;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_personal_edit);
        Toolbar toolbar = findViewById(R.id.toolbar);
        fm = findViewById(R.id.fm);
        name = findViewById(R.id.name);
        ot = findViewById(R.id.ot);
        date = findViewById(R.id.date);
        phone = findViewById(R.id.number);
        email = findViewById(R.id.email);
        mode = getIntent().getIntExtra(MODE_EXTRA, MODE_DEFAULT);
        setSupportActionBar(toolbar);
        if (mode == 1) toolbar.setTitle("Регистрация");
        getSupportActionBar().setHomeButtonEnabled(mode == 0);
        getSupportActionBar().setDisplayHomeAsUpEnabled(mode == 0);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_accept, menu);
        return true;
    }

    public void onBackPressed() {
        Intent data = new Intent();
        setResult(RESULT_OK, data);

        super.finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onMenuHomePressed();
                return true;
            case R.id.action_accept:
                //Переход на экран
                if (mode == 0) {
                    Intent data = new Intent();
                    setResult(RESULT_OK, data);
                } else {
                    Intent intent = new Intent(this, MainAddressEdit.class);
                    ClientRequest request = new ClientRequest(fm.getText().toString(), name.getText().toString(),
                            ot.getText().toString(), email.getText().toString(), phone.getText().toString());
                    GsonBuilder gsonBuilder = new GsonBuilder();
                    String requestJson = gsonBuilder.create().toJson(request);
                    intent.putExtra(MainAddressEdit.MODE_EXTRA, MainAddressEdit.MODE_REG);
                    intent.putExtra(MainAddressEdit.USER_EXTRA, requestJson);
                    startActivity(intent);
                }

                super.finish();
        }
        return super.onOptionsItemSelected(item);
    }

    protected void onMenuHomePressed() {
        onBackPressed();
    }
}
