package com.wsplite.zkhrequestsmobile.ui.subcategory;

import android.content.Context;
import android.os.Bundle;

import android.content.Intent;

import com.wsplite.zkhrequestsmobile.R;
import com.wsplite.zkhrequestsmobile.ui.request.RequestActivity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class CategoriesActivity extends AppCompatActivity {

    public final static String CATEGORY_EXTRA = "category_extra";
    public final static String MODE_EXTRA = "mode_extra";

    public final static int MODE_DEFAULT = 0;
    public final static int MODE_ALL = 1;

    private int mode = 0;

    private CategoriesRvAdapter adapter;
    private List<CategoryItem> listCategory;

    private CategoriesPresenter presenter = new CategoriesPresenter(this);

    public static void start(Context context, String category) {
        Intent intent = new Intent(context, CategoriesActivity.class);
        intent.putExtra(CATEGORY_EXTRA, category);
        intent.putExtra(MODE_EXTRA, MODE_DEFAULT);
        context.startActivity(intent);
    }

    public static Intent getIntent(Context context) {
        Intent intent = new Intent(context, CategoriesActivity.class);
        intent.putExtra(MODE_EXTRA, MODE_ALL);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_categories);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        RecyclerView rvCategory = findViewById(R.id.rv_category);

        String categoryId = getIntent().getStringExtra(CATEGORY_EXTRA);
        mode = getIntent().getIntExtra(MODE_EXTRA, 0);

        adapter = new CategoriesRvAdapter(new ArrayList<>(), category -> {
            if (mode == 1) {
                Intent intent = new Intent();
                intent.putExtra("name", category.getCategory());
                intent.putExtra("id", category.getId());
                setResult(RESULT_OK, intent);
                finish();
            } else {
                RequestActivity.start(this, categoryId, category.getCategory());
            }
        });
        rvCategory.setLayoutManager(new LinearLayoutManager(this));
        rvCategory.setAdapter(adapter);
        presenter.onCreate(getIntent().getStringExtra(CATEGORY_EXTRA), getIntent().getIntExtra(MODE_EXTRA, 0));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.search, menu);
        final MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) searchItem.getActionView();

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (listCategory != null) {
                    List<CategoryItem> items = new ArrayList<>();
                    for (CategoryItem item : listCategory) {
                        if (item.getCategory().toLowerCase().contains(newText.toLowerCase())) {
                            items.add(item);
                        }
                    }
                    adapter.setCategories(items);
                }
                return true;
            }
        });
        return true;
    }

    public void setCategories(List<CategoryItem> items) {
        listCategory = items;
        adapter.setCategories(items);
    }

    public void showLoader() {
        findViewById(R.id.pb_categories).setVisibility(View.VISIBLE);
    }

    public void hideLoader() {
        findViewById(R.id.pb_categories).setVisibility(View.GONE);
    }

    public void toast(String text) {
        Toast.makeText(this, text, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onBackPressed() {
        Intent data = new Intent();
        setResult(RESULT_OK, data);

        super.finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onMenuHomePressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    protected void onMenuHomePressed() {
        onBackPressed();
    }
}
