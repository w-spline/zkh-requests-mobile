package com.wsplite.zkhrequestsmobile.ui.subcategory;

import com.wsplite.zkhrequestsmobile.domain.CategoriesInteractor;
import com.wsplite.zkhrequestsmobile.domain.InteractorsFactory;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.internal.disposables.DisposableContainer;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Evgeniy Mezentsev on 2019-07-28.
 * email: emezentsev@ccsteam.ru
 */
public class CategoriesPresenter {

    private CategoriesActivity activity;
    private CategoriesInteractor categoriesInteractor = InteractorsFactory.INSTANCE.getCategoriesInteractor();

    private DisposableContainer container = new CompositeDisposable();

    public CategoriesPresenter(CategoriesActivity activity) {
        this.activity = activity;
    }

    public void onCreate(String id, int mode) {
        if (mode == 0) {
            loadCategories(id);
        } else {
            loadAll();
        }
    }

    private void loadCategories(String id) {
        container.add(categoriesInteractor.getListRequest(id)
                .doOnSubscribe(data -> activity.showLoader())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        list -> {
                            activity.hideLoader();
                            activity.setCategories(list);
                        },
                        error -> activity.toast(error.getMessage())
                ));
    }

    private void loadAll() {
        container.add(categoriesInteractor.getListRequest()
                .doOnSubscribe(data -> activity.showLoader())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        list -> {
                            activity.hideLoader();
                            activity.setCategories(list);
                        },
                        error -> activity.toast(error.getMessage())
                ));
    }
}
