package com.wsplite.zkhrequestsmobile.ui.main.category;

import com.wsplite.zkhrequestsmobile.domain.CategoriesInteractor;
import com.wsplite.zkhrequestsmobile.domain.InteractorsFactory;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.internal.disposables.DisposableContainer;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Evgeniy Mezentsev on 2019-07-28.
 * email: emezentsev@ccsteam.ru
 */
public class MainCategoryPresenter {

    private MainCategoryFragment fragment;
    private CategoriesInteractor categoriesInteractor = InteractorsFactory.INSTANCE.getCategoriesInteractor();

    private DisposableContainer container = new CompositeDisposable();

    public MainCategoryPresenter(MainCategoryFragment fragment) {
        this.fragment = fragment;
    }

    public void onCreate() {
        getCategories();
    }

    private void getCategories() {
        container.add(categoriesInteractor.getMainCategories()
                .doOnSubscribe(data -> fragment.showLoader())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        list -> {
                            fragment.hideLoader();
                            fragment.setList(list);
                        },
                        error -> fragment.toast(error.getMessage())
                ));
    }

}
