package com.wsplite.zkhrequestsmobile.ui.component;

import androidx.annotation.DrawableRes;

import com.wsplite.zkhrequestsmobile.R;

/**
 * Created by Evgeniy Mezentsev on 2019-07-28.
 * email: emezentsev@ccsteam.ru
 */
public class RequestStatusColor {

    public final static String NEW = "новая";
    public final static String COMPLETE = "выполнено";
    public final static String REJECTED = "отклонено";
    public final static String REGISTERED = "зарегестрировано";
    public final static String EXTENDED = "продлена";

    public static @DrawableRes int getBackground(String status) {
        switch (status.toLowerCase()) {
            case NEW:
                return R.drawable.bg_oval_blue;
            case COMPLETE:
                return R.drawable.bg_oval_green;
            case REGISTERED:
                return R.drawable.bg_oval_yellow;
            case REJECTED:
                return R.drawable.bg_oval_red;
            case EXTENDED:
                return R.drawable.bg_oval_grey;
            default:
                return 0;
        }
    }

}
