package com.wsplite.zkhrequestsmobile.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.wsplite.zkhrequestsmobile.R;
import com.wsplite.zkhrequestsmobile.data.Singleton;
import com.wsplite.zkhrequestsmobile.ui.main.MainActivity;
import com.wsplite.zkhrequestsmobile.ui.main.settings.personal.MainPersonalEditActivity;

public class SpashScreenActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spash_screen);
        if (Singleton.INSTANCE.id == null || Singleton.INSTANCE.id.isEmpty()) {
            Intent intent = new Intent(this, MainPersonalEditActivity.class);
            intent.putExtra(MainPersonalEditActivity.MODE_EXTRA, MainPersonalEditActivity.MODE_REG);
            startActivity(intent);
        } else {
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
        }
    }

}
