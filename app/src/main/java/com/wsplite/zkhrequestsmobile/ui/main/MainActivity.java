package com.wsplite.zkhrequestsmobile.ui.main;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.aurelhubert.ahbottomnavigation.AHBottomNavigation;
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationAdapter;
import com.wsplite.zkhrequestsmobile.R;
import com.wsplite.zkhrequestsmobile.ui.subcategory.CategoriesActivity;
import com.wsplite.zkhrequestsmobile.ui.main.requests.RequestListFragment;
import com.wsplite.zkhrequestsmobile.ui.main.category.MainCategoryFragment;
import com.wsplite.zkhrequestsmobile.ui.main.category.item.MainCategoryItem;
import com.wsplite.zkhrequestsmobile.ui.main.settings.MainSettingsFragment;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Evgeniy Mezentsev on 2019-07-27.
 * email: emezentsev@ccsteam.ru
 */
public class MainActivity extends AppCompatActivity implements MainCategoryFragment.OnListFragmentInteractionListener {

    private static final String PROBLEMS = "PROBLEBS";
    private static final String REQUESTS = "REQUESTS";
    private static final String SETTINGS = "SETTINGS";
    public static final int PROBLEMS_INDEX = 0;
    public static final int REQUESTS_INDEX = 1;
    public static final int SETTINGS_INDEX = 2;

    @BindView(R.id.bottom_navigation) AHBottomNavigation bottomNavigation;

    private Fragment currentFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        setupBottomNavigation();
        showFragment(PROBLEMS, MainCategoryFragment.newInstance());
    }

    private void setupBottomNavigation() {
        bottomNavigation.setColored(false);
        bottomNavigation.setForceTint(true);
        bottomNavigation.setBehaviorTranslationEnabled(false);
        bottomNavigation.setOnTabSelectedListener(this::onTabSelected);

        bottomNavigation.setAccentColor(ContextCompat.getColor(this, R.color.white));
        bottomNavigation.setInactiveColor(ContextCompat.getColor(this, R.color.light_white));

        bottomNavigation.setDefaultBackgroundColor(ContextCompat.getColor(this, R.color.colorMain));
        bottomNavigation.setCurrentItem(0);
        bottomNavigation.setTitleState(AHBottomNavigation.TitleState.SHOW_WHEN_ACTIVE);

        AHBottomNavigationAdapter navigationAdapter = new AHBottomNavigationAdapter(this, R.menu.main_bottom_bar);
        navigationAdapter.setupWithBottomNavigation(bottomNavigation);
    }

    private boolean onTabSelected(int position, boolean wasSelected) {
        if (wasSelected) {
            return false;
        }
        switch (position) {
            case PROBLEMS_INDEX:
                showFragment(PROBLEMS, MainCategoryFragment.newInstance());
                return true;
            case REQUESTS_INDEX:
                showFragment(REQUESTS, RequestListFragment.newInstance());
                return true;
            case SETTINGS_INDEX:
                showFragment(SETTINGS, MainSettingsFragment.newInstance());
                return true;
            default:
                return false;
        }
    }

    private void showFragment(String tag, Fragment fragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        if (currentFragment != null && currentFragment.isVisible()) {
            transaction.hide(currentFragment);
        }

        Fragment newFragment = getSupportFragmentManager().findFragmentByTag(tag);
        if (newFragment == null) {
            newFragment = fragment;
            transaction.add(R.id.content_frame, newFragment, tag);
        }
        currentFragment = newFragment;

        transaction.show(newFragment);
        try {
            transaction.commitNow();
        } catch (IllegalStateException ignored) {
        }
    }

    @Override
    public void onListFragmentInteraction(MainCategoryItem item) {
        CategoriesActivity.start(this, item.getId());
    }
}
